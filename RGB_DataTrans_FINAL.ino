
#include <Wire.h>
#include "Adafruit_TCS34725.h"

// Define the pin to transmit data
#define DATA_PIN 2
#define CLOCK_PIN 6

// Define the dimensions of the data array
#define ROWS 8
#define COLS 3

byte muxAddress = 0x70;
boolean transmit;

float r, g, b;

uint16_t data[ROWS][COLS];

Adafruit_TCS34725 tcs[] = {Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_1X),
                           Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_1X),
                           Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_1X),
                           Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_1X),
                           Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_1X),
                           Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_1X),
                           Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_1X),
                           Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_1X)};

void setup(){
    Serial.begin(9600);
    delay(1000);
    pinMode(DATA_PIN, OUTPUT); // pin 2 as output
    Wire.begin();
    pinMode(LED_BUILTIN,OUTPUT);  // sets onBoard LED as output
    delay(1000);
    initColorSensors();
}


void loop(){
    for(int i = 0; i < 8; i++){
      readColors(i);
      
      /* Serial.print("\nSensor #");    
      Serial.println(i + 1);
      Serial.print("  R:"); Serial.print(data[i][0], DEC); Serial.print("  "); 
      Serial.print("G:"); Serial.print(data[i][1], DEC); Serial.print("  ");
      Serial.print("B:"); Serial.print(data[i][2], DEC); Serial.print("  "); */
    }
    Serial.println("\n\n");
    delay(1000);

    // print data array
    for(int i = 0; i < 8; i++) {
      //Serial.print("\n"); 
      Serial.print("Sensor #");    
      Serial.print(i+1);
      Serial.print(":  "); 
      for (int j = 0; j < 3; j++) {
        Serial.print(" ");
        Serial.print(data[i][j]);
      }
      Serial.println(" ");
    }
    Serial.println(" ");
    //transmitData();
      // indicate start with 4 zero bits
      sendStartSignal();
      Serial.println("start signal sent");
      // Iterate through each element in the data array
      for (int row = 0; row < ROWS; row++) {
        for (int col = 0; col < COLS; col++) {
          int value = data[row][col];
          
          // Send each bit of the value
          for (int i = 3; i >= 0; i--) {
            digitalWrite(CLOCK_PIN, HIGH);
            delayMicroseconds(5); // Adjust this delay as needed
            int bit = (value >> i) & 1;
            digitalWrite(DATA_PIN, bit);
            Serial.print(bit);
            delayMicroseconds(5); // Adjust this delay as needed
            digitalWrite(CLOCK_PIN, LOW);
            //Serial.print("clock goes low");
            delayMicroseconds(5); // Adjust this delay as needed
          }
          Serial.print(" ");
        }
        Serial.println("  ");
      }
}

void initColorSensors(){
    for(int i = 0; i < 8; i++){
        chooseBus(i);
        if (tcs[i].begin()){
            Serial.print("Found sensor "); Serial.println(i+1);
        } else{
            Serial.println("No Sensor Found");
            // while (true);
        }
    }
}

void readColors(byte sensorNum){
    chooseBus(sensorNum);
    uint16_t red, green, blue, clear;
    tcs[sensorNum].setInterrupt(false);
    delay(60); // Time to capture the color
    tcs[sensorNum].getRawData(&red, &green, &blue, &clear); // reading the rgb values 16bits at a time from the i2c channel
    tcs[sensorNum].setInterrupt(true);
    
    //Serial.println(red);
    //Serial.println(green);
    //Serial.println(blue);
    //Serial.println(clear);

    getRGB(red, green, blue, clear); // processing by dividng by clear value and then multiplying by 256

    //Serial.println("\n\n");
    //Serial.println(r);
    //Serial.println(g);
    //Serial.println(b);
    if (sensorNum == 2) {
      data[5][0] = r;
      data[5][1] = g;
      data[5][2] = b;
    } else if (sensorNum == 5) {
      data[2][0] = r;
      data[2][1] = g;
      data[2][2] = b;
    } else {
      data[sensorNum][0] = r;
      data[sensorNum][1] = g;
      data[sensorNum][2] = b;
    }
}

void getRGB(uint16_t red, uint16_t green, uint16_t blue, uint32_t clear){
        // getting rid of IR component of light
    r = ((float)red / clear * 16);
    g = ((float)green / clear * 16);
    b = ((float)blue / clear * 16);
}
    
void chooseBus(uint8_t bus){
    Wire.beginTransmission(0x70);
    Wire.write(1 << bus);
    Wire.endTransmission();
}

void sendStartSignal() {
    digitalWrite(CLOCK_PIN, HIGH);
    delayMicroseconds(5); // Adjust this delay as needed
    digitalWrite(DATA_PIN, LOW);
    delayMicroseconds(5); // Adjust this delay as needed
    digitalWrite(CLOCK_PIN, LOW);
    delayMicroseconds(5); // Adjust this delay as needed
}

 
void transmitData() {
  digitalWrite(DATA_PIN, HIGH); // 1 start bit
  Serial.println(HIGH);
  // Iterate through each element in the data array
  for (int row = 0; row < ROWS; row++) {
    for (int col = 0; col < COLS; col++) {
      int value = data[row][col];
      
      // Send each bit of the value
      for (int i = 3; i >= 0; i--) {
        delay(5);
        // Extract the i-th bit from the value
        digitalWrite(CLOCK_PIN, HIGH);
        int bit = (value >> i) & 1;
        // Send the bit through the DATA_PIN
        digitalWrite(DATA_PIN, bit);
        Serial.print(bit);

        delay(5);
        digitalWrite(CLOCK_PIN, LOW);
      }
      Serial
    }
  }
}
