`timescale 1ns / 1ps
/**
 * 
 * READ THIS DESCRIPTION:
 *
 * This is the Wrapper module that will serve as the header file combining your processor, 
 * RegFile and Memory elements together.
 *
 * This file will be used to generate the bitstream to upload to the FPGA.
 * We have provided a sibling file, Wrapper_tb.v so that you can test your processor's functionality.
 * 
 * We will be using our own separate Wrapper_tb.v to test your code. You are allowed to make changes to the Wrapper files 
 * for your own individual testing, but we expect your final processor.v and memory modules to work with the 
 * provided Wrapper interface.
 * 
 * Refer to Lab 5 documents for detailed instructions on how to interface 
 * with the memory elements. Each imem and dmem modules will take 12-bit 
 * addresses and will allow for storing of 32-bit values at each address. 
 * Each memory module should receive a single clock. At which edges, is 
 * purely a design choice (and thereby up to you). 
 * 
 * You must change line 36 to add the memory file of the test you created using the assembler
 * For example, you would add sample inside of the quotes on line 38 after assembling sample.s
 *
 **/

module Wrapper (CLK100MHZ, reset, BTNC, SW, hSync, vSync, VGA_R, VGA_G, VGA_B, ps2_clk, ps2_data);
	input CLK100MHZ, reset, BTNC;
	input[3:0] SW;
	output hSync, vSync;
	output[3:0] VGA_R, VGA_G, VGA_B;
	inout ps2_clk, ps2_data;

	wire clock;
	assign clock = clk25;

	////////////////////////////////////////////// WRAPPER //////////////////////////////////////////////

	wire rwe, mwe;
	wire[4:0] rd, rs1, rs2;
	wire[31:0] instAddr, instData, 
		rData, regA, regB,
		memAddr, memDataIn, memDataOut;
	wire[31:0] reg1out, reg2out, reg3out, reg4out, reg5out, reg6out, reg7out, reg8out, 
		 reg9out, reg10out, reg11out, reg12out, reg13out, reg14out, reg15out, reg16out;
	

	// ADD YOUR MEMORY FILE HERE
	localparam INSTR_FILE = "scramble";
	
	// Main Processing Unit
	processor CPU(.clock(clock), .reset(!reset), 
								
		// ROM
		.address_imem(instAddr), .q_imem(instData),
									
		// Regfile
		.ctrl_writeEnable(rwe),     .ctrl_writeReg(rd),
		.ctrl_readRegA(rs1),     .ctrl_readRegB(rs2), 
		.data_writeReg(rData), .data_readRegA(regA), .data_readRegB(regB),
									
		// RAM
		.wren(mwe), .address_dmem(memAddr), 
		.data(memDataIn), .q_dmem(memDataOut)); 
	
	// Instruction Memory (ROM)
	ROM #(.MEMFILE({INSTR_FILE, ".mem"}))
	InstMem(.clk(clock), 
		.addr(instAddr[11:0]), 
		.dataOut(instData));
	
	// Register File
	regfile RegisterFile(.clock(clock), 
		.ctrl_writeEnable(rwe), .ctrl_reset(!reset), 
		.ctrl_writeReg(rd),
		.ctrl_readRegA(rs1), .ctrl_readRegB(rs2), 
		.data_writeReg(rData), .data_readRegA(regA), .data_readRegB(regB),
		.reg1out(reg1out), .reg2out(reg2out), .reg3out(reg3out), .reg4out(reg4out),
		.reg5out(reg5out), .reg6out(reg6out), .reg7out(reg7out), .reg8out(reg8out),
		.reg9out(reg9out), .reg10out(reg10out), .reg11out(reg11out), .reg12out(reg12out),
		.reg13out(reg13out), .reg14out(reg14out), .reg15out(reg15out), .reg16out(reg16out));
						
	// Processor Memory (RAM)
	RAM ProcMem(.clk(clock), 
		.wEn(mwe), 
		.addr(memAddr[11:0]), 
		.dataIn(memDataIn), 
		.dataOut(memDataOut));

	////////////////////////////////////////////// VGA //////////////////////////////////////////////

	// Lab Memory Files Location
	localparam FILES_PATH = "C:/Users/rno5/Downloads/updated_processor/";

	// Clock divider 100 MHz -> 25 MHz
	wire clk25; // 25MHz clock
	reg[1:0] pixCounter = 0;      // Pixel counter to divide the clock
	assign clk25 = pixCounter[1]; // Set the clock high whenever the second bit (2) is high
	always @(posedge CLK100MHZ) begin
		pixCounter <= pixCounter + 1; // Since the reg is only 3 bits, it will reset every 8 cycles
	end

	// VGA Timing Generation for a Standard VGA Screen
	localparam 
		VIDEO_WIDTH = 640,  // Standard VGA Width
		VIDEO_HEIGHT = 480; // Standard VGA Height
    
    wire[9:0] x;
	wire[8:0] y;
	wire active, screenEnd;

	VGATimingGenerator #(
		.HEIGHT(VIDEO_HEIGHT), // Use the standard VGA Values
		.WIDTH(VIDEO_WIDTH))
	Display( 
		.clk25(clk25),  	   // 25MHz Pixel Clock
		.reset(BTNC),		   // Reset Signal
		.screenEnd(screenEnd), // High for one cycle when between two frames
		.active(active),	   // High when drawing pixels
		.hSync(hSync),  	   // Set Generated H Signal
		.vSync(vSync),		   // Set Generated V Signal
		.x(x), 				   // X Coordinate (from left)
		.y(y)); 			   // Y Coordinate (from top)	
	
	// motion controls
	wire up, down, left, right;
	assign up = SW[0];
	assign down = SW[1];
	assign left = SW[2];
	assign right = SW[3];

	/// YOUR CODE STARTS HERE ///
	reg[9:0] square1x, square2x, square3x, square4x, square5x, square6x, square7x, square8x, square9x,
             square1y, square2y, square3y, square4y, square5y, square6y, square7y, square8y, square9y;
	// create registers for the locations of each of the 9 squares (top left coordinate)
	always begin
        square1x <= reg1out[9:0];
        square2x <= reg3out[9:0];
        square3x <= reg5out[9:0];
        square4x <= reg7out[9:0];
        square5x <= reg9out[9:0];
        square6x <= reg11out[9:0];
        square7x <= reg13out[9:0];
        square8x <= reg15out[9:0];
        square9x <= 10'd420;
    
        square1y <= reg2out[9:0];
        square2y <= reg4out[9:0];
        square3y <= reg6out[9:0];
        square4y <= reg8out[9:0];
        square5y <= reg10out[9:0];
        square6y <= reg12out[9:0];
        square7y <= reg14out[9:0];
        square8y <= reg16out[9:0];
        square9y <= 10'd320;
    end
  

	// Image Data to Map Pixel Location to Color Address
	localparam 
		PIXEL_COUNT = VIDEO_WIDTH*VIDEO_HEIGHT, 	             // Number of pixels on the screen
		PIXEL_ADDRESS_WIDTH = $clog2(PIXEL_COUNT) + 1,           // Use built in log2 command
		BITS_PER_COLOR = 12; 	  								 // Nexys A7 uses 12 bits/color

    // booleans to define coordinate range of each square
    wire isSquare1, isSquare2, isSquare3, isSquare4, isSquare5, isSquare6, isSquare7, isSquare8, isSquare9, background;
    assign isSquare1 = ((x >= square1x) && (x <= (square1x + 10'd150))) && ((y >= square1y) && (y <= (square1y + 10'd150)));
    assign isSquare2 = ((x >= square2x) && (x <= (square2x + 10'd150))) && ((y >= square2y) && (y <= (square2y + 10'd150)));
    assign isSquare3 = ((x >= square3x) && (x <= (square3x + 10'd150))) && ((y >= square3y) && (y <= (square3y + 10'd150)));
    assign isSquare4 = ((x >= square4x) && (x <= (square4x + 10'd150))) && ((y >= square4y) && (y <= (square4y + 10'd150)));
    assign isSquare5 = ((x >= square5x) && (x <= (square5x + 10'd150))) && ((y >= square5y) && (y <= (square5y + 10'd150)));
    assign isSquare6 = ((x >= square6x) && (x <= (square6x + 10'd150))) && ((y >= square6y) && (y <= (square6y + 10'd150)));
    assign isSquare7 = ((x >= square7x) && (x <= (square7x + 10'd150))) && ((y >= square7y) && (y <= (square7y + 10'd150)));
    assign isSquare8 = ((x >= square8x) && (x <= (square8x + 10'd150))) && ((y >= square8y) && (y <= (square8y + 10'd150)));
    assign isSquare9 = ((x >= square9x) && (x <= (square9x + 10'd150))) && ((y >= square9y) && (y <= (square9y + 10'd150)));
    assign background = !isSquare1 && !isSquare2 && !isSquare3 && !isSquare4 && !isSquare5 && !isSquare6 && !isSquare7 && !isSquare8 && !isSquare9;
    
    //create registers to hold color bits
    reg [BITS_PER_COLOR-1:0] rSensor1, rSensor2, rSensor3, rSensor4, rSensor5, rSensor6, rSensor7, rSensor8, rSensor9;
    
    // for now, we will assign random colors to regs
	// read sensor data here
    always @(posedge screenEnd) begin
        rSensor1 <= 12'heee;
        rSensor2 <= 12'habc;
        rSensor3 <= 12'h644;
        rSensor4 <= 12'h099;
        rSensor5 <= 12'h821;
        rSensor6 <= 12'hd45;
        rSensor7 <= 12'he00;
        rSensor8 <= 12'h0e0;
        rSensor9 <= 12'h000;
    end
    
    // wires hold color at each square space
	wire[BITS_PER_COLOR-1:0] drawSquare1, drawSquare2, drawSquare3, drawSquare4, drawSquare5, drawSquare6, drawSquare7, drawSquare8, drawSquare9;
    wire[BITS_PER_COLOR-1:0] colorOut;
    assign drawSquare1 = isSquare1 ? rSensor1 : 12'd0;
    assign drawSquare2 = isSquare2 ? rSensor2 : drawSquare1;
    assign drawSquare3 = isSquare3 ? rSensor3 : drawSquare2;
    assign drawSquare4 = isSquare4 ? rSensor4 : drawSquare3;
    assign drawSquare5 = isSquare5 ? rSensor5 : drawSquare4;
    assign drawSquare6 = isSquare6 ? rSensor6 : drawSquare5;
    assign drawSquare7 = isSquare7 ? rSensor7 : drawSquare6;
    assign drawSquare8 = isSquare8 ? rSensor8 : drawSquare7;
    assign drawSquare9 = isSquare9 ? rSensor9 : drawSquare8;
    assign colorActive = background ? 12'h000 : drawSquare9;
	assign colorOut = active ? drawSquare9 : 12'd0; // When not active, output black

    /*

	// Controls to solve puzzle goes here
	wire [2:0] block1u, block1l, block2u, block2l, block2r, block3u, block3r, block4u, block4d, block4l,
			  block5u, block5d, block5l, block5r, block6u, block6d, block6r, block7d, block7l, 
			  block8d, block8l, block8r, block9d, block9r;
	block_finder s1u(10'd100, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y,
					block1u);
	block_finder s1l(10'd260, 10'd0, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y,
					block1l);
	block_finder s2u(10'd260, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block2u);
	block_finder s2l(10'd420, 10'd0, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block2l);
	block_finder s2r(10'd100, 10'd0, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block2r);
	block_finder s3u(10'd420, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block3u);
	block_finder s3r(10'd260, 10'd0, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block3r);
	block_finder s4u(10'd100, 10'd320, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block4u);
	block_finder s4d(10'd100, 10'd0, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block4d);
	block_finder s4l(10'd260, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block4l);
	block_finder s5u(10'd260, 10'd320, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block5u);
	block_finder s5d(10'd260, 10'd0, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block5d);
	block_finder s5l(10'd420, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block5l);
	block_finder s5r(10'd100, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block5r);
	block_finder s6u(10'd420, 10'd320, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block6u);
	block_finder s6d(10'd420, 10'd0, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block6d);
	block_finder s6r(10'd260, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block6r);
	block_finder s7d(10'd100, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block7d);
	block_finder s7l(10'd260, 10'd320, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block7l);
	block_finder s8d(10'd260, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block8d);
	block_finder s8l(10'd420, 10'd320, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block8l);
	block_finder s8r(10'd100, 10'd320, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block8r);
	block_finder s9d(10'd420, 10'd160, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block9d);
	block_finder s9r(10'd260, 10'd320, 
					square1x, square1y, square2x, square2y, square3x, square3y, square4x, square4y,
    				square5x, square5y, square6x, square6y, square7x, square7y, square8x, square8y, 
					block9r);
	always @(posedge clock) begin
        if (square9x == 10'd100 && square9y == 10'd0 && up) begin
			square9x <= 10'd100;
			square9y <= 10'd160;
			case (block1u)
                3'd1: begin
                    square1x <= 10'd100;
                    square1y <= 10'd0;
                end
                3'd2: begin
                    square2x <= 10'd100;
                    square2y <= 10'd0;
                end
                3'd3: begin
                    square3x <= 10'd100;
                    square3y <= 10'd0;
                end
                3'd4: begin
                    square4x <= 10'd100;
                    square4y <= 10'd0;
                end
                3'd5: begin
                    square5x <= 10'd100;
                    square5y <= 10'd0;
                end
                3'd6: begin
                    square6x <= 10'd100;
                    square6y <= 10'd0;
                end
                3'd7: begin
                    square7x <= 10'd100;
                    square7y <= 10'd0;
                end
                3'd8: begin
                    square8x <= 10'd100;
                    square8y <= 10'd0;
                end
                default: begin
                    // No block to update
                end
            endcase	 	
        end
		else if (square9x == 10'd100 && square9y == 10'd0 && left) begin
            square9x <= 10'd260;
			square9y <= 10'd0;
			case (block1l)
                3'd1: begin
                    square1x <= 10'd100;
                    square1y <= 10'd0;
                end
                3'd2: begin
                    square2x <= 10'd100;
                    square2y <= 10'd0;
                end
                3'd3: begin
                    square3x <= 10'd100;
                    square3y <= 10'd0;
                end
                3'd4: begin
                    square4x <= 10'd100;
                    square4y <= 10'd0;
                end
                3'd5: begin
                    square5x <= 10'd100;
                    square5y <= 10'd0;
                end
                3'd6: begin
                    square6x <= 10'd100;
                    square6y <= 10'd0;
                end
                3'd7: begin
                    square7x <= 10'd100;
                    square7y <= 10'd0;
                end
                3'd8: begin
                    square8x <= 10'd100;
                    square8y <= 10'd0;
                end
                default: begin
                    // No block to update
                end
            endcase	
        end


		else if (square9x == 10'd260 && square9y == 10'd0 && up) begin
            square9x <= 10'd260;
			square9y <= 10'd160;
            case (block2u)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd0;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd0;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd0;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd0;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd0;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd0;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd0;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd0;
                end
                default: begin
                    // No block to update
                end
            endcase			
        end
		else if (square9x == 10'd260 && square9y == 10'd0 && left) begin
            square9x <= 10'd420;
			square9y <= 10'd0;
			case (block2l)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd0;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd0;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd0;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd0;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd0;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd0;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd0;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd0;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd260 && square9y == 10'd0 && right) begin
            square9x <= 10'd100;
			square9y <= 10'd0;
			case (block2r)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd0;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd0;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd0;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd0;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd0;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd0;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd0;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd0;
                end
                default: begin
                    // No block to update
                end
            endcase
        end


		else if (square9x == 10'd420 && square9y == 10'd0 && up) begin
            square9x <= 10'd420;
			square9y <= 10'd160;
			case (block3u)
                3'd1: begin
                    square1x <= 10'd420;
                    square1y <= 10'd0;
                end
                3'd2: begin
                    square2x <= 10'd420;
                    square2y <= 10'd0;
                end
                3'd3: begin
                    square3x <= 10'd420;
                    square3y <= 10'd0;
                end
                3'd4: begin
                    square4x <= 10'd420;
                    square4y <= 10'd0;
                end
                3'd5: begin
                    square5x <= 10'd420;
                    square5y <= 10'd0;
                end
                3'd6: begin
                    square6x <= 10'd420;
                    square6y <= 10'd0;
                end
                3'd7: begin
                    square7x <= 10'd420;
                    square7y <= 10'd0;
                end
                3'd8: begin
                    square8x <= 10'd420;
                    square8y <= 10'd0;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd420 && square9y == 10'd0 && right) begin
            square9x <= 10'd260;
			square9y <= 10'd0;
			case (block3r)
                3'd1: begin
                    square1x <= 10'd420;
                    square1y <= 10'd0;
                end
                3'd2: begin
                    square2x <= 10'd420;
                    square2y <= 10'd0;
                end
                3'd3: begin
                    square3x <= 10'd420;
                    square3y <= 10'd0;
                end
                3'd4: begin
                    square4x <= 10'd420;
                    square4y <= 10'd0;
                end
                3'd5: begin
                    square5x <= 10'd420;
                    square5y <= 10'd0;
                end
                3'd6: begin
                    square6x <= 10'd420;
                    square6y <= 10'd0;
                end
                3'd7: begin
                    square7x <= 10'd420;
                    square7y <= 10'd0;
                end
                3'd8: begin
                    square8x <= 10'd420;
                    square8y <= 10'd0;
                end
                default: begin
                    // No block to update
                end
            endcase
        end


		else if (square9x == 10'd100 && square9y == 10'd160 && up) begin
            square9x <= 10'd100;
			square9y <= 10'd320;
			case (block4u)
                3'd1: begin
                    square1x <= 10'd100;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd100;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd100;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd100;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd100;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd100;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd100;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd100;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd100 && square9y == 10'd160 && down) begin
            square9x <= 10'd100;
			square9y <= 10'd0;
			case (block4d)
                3'd1: begin
                    square1x <= 10'd100;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd100;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd100;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd100;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd100;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd100;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd100;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd100;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd100 && square9y == 10'd160 && left) begin
            square9x <= 10'd260;
			square9y <= 10'd160;
			case (block4l)
                3'd1: begin
                    square1x <= 10'd100;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd100;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd100;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd100;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd100;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd100;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd100;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd100;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end


		else if (square9x == 10'd260 && square9y == 10'd160 && up) begin
            square9x <= 10'd260;
			square9y <= 10'd320;
			case (block5u)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd260 && square9y == 10'd160 && down) begin
            square9x <= 10'd260;
			square9y <= 10'd0;
			case (block5d)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd260 && square9y == 10'd160 && left) begin
            square9x <= 10'd420;
			square9y <= 10'd160;
			case (block5l)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd260 && square9y == 10'd160 && right) begin
            square9x <= 10'd100;
			square9y <= 10'd160;
			case (block5r)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end


		else if (square9x == 10'd420 && square9y == 10'd160 && up) begin
            square9x <= 10'd420;
			square9y <= 10'd320;
			case (block6u)
                3'd1: begin
                    square1x <= 10'd420;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd420;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd420;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd420;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd420;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd420;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd420;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd420;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd420 && square9y == 10'd160 && down) begin
            square9x <= 10'd420;
			square9y <= 10'd0;
			case (block6d)
                3'd1: begin
                    square1x <= 10'd420;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd420;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd420;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd420;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd420;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd420;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd420;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd420;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd420 && square9y == 10'd160 && right) begin
            square9x <= 10'd260;
			square9y <= 10'd160;
			case (block6r)
                3'd1: begin
                    square1x <= 10'd420;
                    square1y <= 10'd160;
                end
                3'd2: begin
                    square2x <= 10'd420;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd420;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd420;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd420;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd420;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd420;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd420;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end


		else if (square9x == 10'd100 && square9y == 10'd320 && down) begin
            square9x <= 10'd100;
			square9y <= 10'd160;
			case (block7d)
                3'd1: begin
                    square1x <= 10'd100;
                    square1y <= 10'd320;
                end
                3'd2: begin
                    square2x <= 10'd420;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd420;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd420;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd420;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd420;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd420;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd420;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd100 && square9y == 10'd320 && left) begin
            square9x <= 10'd260;
			square9y <= 10'd320;
			case (block7l)
                3'd1: begin
                    square1x <= 10'd100;
                    square1y <= 10'd320;
                end
                3'd2: begin
                    square2x <= 10'd420;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd420;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd420;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd420;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd420;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd420;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd420;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end


		else if (square9x == 10'd260 && square9y == 10'd320 && down) begin
            square9x <= 10'd260;
			square9y <= 10'd160;
			case (block8d)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd320;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd260 && square9y == 10'd320 && left) begin
            square9x <= 10'd420;
			square9y <= 10'd320;
			case (block8l)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd320;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd260 && square9y == 10'd320 && right) begin
            square9x <= 10'd100;
			square9y <= 10'd320;
			case (block8r)
                3'd1: begin
                    square1x <= 10'd260;
                    square1y <= 10'd320;
                end
                3'd2: begin
                    square2x <= 10'd260;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd260;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd260;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd260;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd260;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd260;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd260;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end


		else if (square9x == 10'd420 && square9y == 10'd320 && down) begin
            square9x <= 10'd420;
			square9y <= 10'd160;
			case (block9d)
                3'd1: begin
                    square1x <= 10'd420;
                    square1y <= 10'd320;
                end
                3'd2: begin
                    square2x <= 10'd420;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd420;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd420;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd420;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd420;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd420;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd420;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
		else if (square9x == 10'd420 && square9y == 10'd320 && right) begin
            square9x <= 10'd260;
			square9y <= 10'd320;
			case (block9r)
                3'd1: begin
                    square1x <= 10'd420;
                    square1y <= 10'd320;
                end
                3'd2: begin
                    square2x <= 10'd420;
                    square2y <= 10'd160;
                end
                3'd3: begin
                    square3x <= 10'd420;
                    square3y <= 10'd160;
                end
                3'd4: begin
                    square4x <= 10'd420;
                    square4y <= 10'd160;
                end
                3'd5: begin
                    square5x <= 10'd420;
                    square5y <= 10'd160;
                end
                3'd6: begin
                    square6x <= 10'd420;
                    square6y <= 10'd160;
                end
                3'd7: begin
                    square7x <= 10'd420;
                    square7y <= 10'd160;
                end
                3'd8: begin
                    square8x <= 10'd420;
                    square8y <= 10'd160;
                end
                default: begin
                    // No block to update
                end
            endcase
        end
    end
    */

	// Quickly assign the output colors to their channels using concatenation
	assign {VGA_R, VGA_G, VGA_B} = colorOut;

endmodule
