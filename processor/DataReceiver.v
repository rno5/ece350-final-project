module DataReceiver(
    input wire clk, //clock signal
    input wire reset, // reset signal
    input wire bit_in, // FPGA PMOD pin, recieves data 1 bit at a time
    output reg[11:0] sensor1,
    output reg[11:0] sensor2,
    output reg[11:0] sensor3,
    output reg[11:0] sensor4,
    output reg[11:0] sensor5,
    output reg[11:0] sensor6,
    output reg[11:0] sensor7,
    output reg[11:0] sensor8
);
    
    localparam DATA_LENGTH = 96;
    
    // Internal counter
    reg [6:0] bit_counter;
    reg [DATA_LENGTH-1:0] data; // holds full string of transmitted bits
    
    always @(posedge clk) begin
        if (bit_in == 1) begin //start bit indicating begin receiving
            bit_counter <= 0;
            data <= 96'b0;
        end else if (bit_counter < DATA_LENGTH) begin
            data <= {data[DATA_LENGTH-2 : 0], bit_in}; // Shift in the received bit
            bit_counter <= bit_counter + 1;
        end
    end
    
    wire data_done;
    assign data_done = (bit_counter == DATA_LENGTH);
    
    // each sensor should be a 12 bit reading (4-4-4) - 8 sets of 12 bits
    always @(posedge data_done) begin
        sensor1 <= data[95:84];
        sensor2 <= data[83:72];
        sensor3 <= data[71:60];
        sensor4 <= data[59:48];
        sensor5 <= data[47:36];
        sensor6 <= data[35:24];
        sensor7 <= data[23:12];
        sensor8 <= data[11:0];
    end
    
endmodule