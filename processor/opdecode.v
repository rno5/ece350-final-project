module opdecode(opcode, arith, addi, sw, lw, j, bne, jal, jr, blt, bex, setx);
    input [4:0] opcode;
    output arith, addi, sw, lw, j, bne, jal, jr, blt, bex, setx;

    /*
        00000 - arithmetic (ALU, mult, div)
            mult = (ALU_op == 00110);
            div = (ALU_op == 00111);
        00001 - j
        00010 - bne
        00011 - jal
        00100 - jr
        00101 - addi
        00110 - blt
        00111 - sw
        01000 - lw
        10110 - bex
        10101 - setx
    */

    assign arith = (opcode == 5'b00000) ? 1'b1 : 1'b0;
    assign addi = (opcode == 5'b00101) ? 1'b1 : 1'b0;
    assign sw = (opcode == 5'b00111) ? 1'b1 : 1'b0;
    assign lw = (opcode == 5'b01000) ? 1'b1 : 1'b0;
    assign j = (opcode == 5'b00001) ? 1'b1 : 1'b0;
    assign bne = (opcode == 5'b00010) ? 1'b1 : 1'b0;
    assign jal = (opcode == 5'b00011) ? 1'b1 : 1'b0;
    assign jr = (opcode == 5'b00100) ? 1'b1 : 1'b0;
    assign blt = (opcode == 5'b00110) ? 1'b1 : 1'b0;
    assign bex = (opcode == 5'b10110) ? 1'b1 : 1'b0;
    assign setx = (opcode == 5'b10101) ? 1'b1 : 1'b0;

endmodule