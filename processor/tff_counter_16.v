module tff_counter_16(data_resultRDY, all_off, clock, reset);
    input clock, reset;
    output data_resultRDY, all_off;

    wire Q1, Q2, Q4, Q8;

    TFF tff1(Q1, 1'b1, clock, reset); 
    TFF tff2(Q2, Q1, clock, reset); 
    TFF tff4(Q4, Q2 & Q1, clock, reset); 
    TFF tff8(Q8, Q4 & Q2 & Q1, clock, reset); 

    assign data_resultRDY = Q8 & Q4 & Q2 & Q1;
    assign all_off = !Q8 & !Q4 & !Q2 & !Q1;

endmodule