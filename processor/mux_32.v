module mux_32 #(parameter WIDTH = 32) (out, select, in);
    input [4:0] select;
    input [31:0][WIDTH -1:0] in;
    output [WIDTH -1:0] out;
    wire [3:0][WIDTH -1:0] w;
    mux_8 first_8(w[3], select[2:0], in[31:24]);
    mux_8 second_8(w[2], select[2:0], in[23:16]);
    mux_8 third_8(w[1], select[2:0], in[15:8]);
    mux_8 last_8(w[0], select[2:0], in[7:0]);
    mux_4 which_mux(out, select[4:3], w[3:0]);
endmodule
