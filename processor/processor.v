/**
 * READ THIS DESCRIPTION!
 *
 * This is your processor module that will contain the bulk of your code submission. You are to implement
 * a 5-stage pipelined processor in this module, accounting for hazards and implementing bypasses as
 * necessary.
 *
 * Ultimately, your processor will be tested by a master skeleton, so the
 * testbench can see which controls signal you active when. Therefore, there needs to be a way to
 * "inject" imem, dmem, and regfile interfaces from some external controller module. The skeleton
 * file, Wrapper.v, acts as a small wrapper around your processor for this purpose. Refer to Wrapper.v
 * for more details.
 *
 * As a result, this module will NOT contain the RegFile nor the memory modules. Study the inputs 
 * very carefully - the RegFile-related I/Os are merely signals to be sent to the RegFile instantiated
 * in your Wrapper module. This is the same for your memory elements. 
 *
 *
 */
module processor(
    // Control signals
    clock,                          // I: The master clock
    reset,                          // I: A reset signal

    // Imem
    address_imem,                   // O: The address of the data to get from imem
    q_imem,                         // I: The data from imem

    // Dmem
    address_dmem,                   // O: The address of the data to get or put from/to dmem
    data,                           // O: The data to write to dmem
    wren,                           // O: Write enable for dmem
    q_dmem,                         // I: The data from dmem

    // Regfile
    ctrl_writeEnable,               // O: Write enable for RegFile
    ctrl_writeReg,                  // O: Register to write to in RegFile
    ctrl_readRegA,                  // O: Register to read from port A of RegFile
    ctrl_readRegB,                  // O: Register to read from port B of RegFile
    data_writeReg,                  // O: Data to write to for RegFile
    data_readRegA,                  // I: Data from port A of RegFile
    data_readRegB                   // I: Data from port B of RegFile
	 
	);

	// Control signals
	input clock, reset;
	
	// Imem
    output [31:0] address_imem;
	input [31:0] q_imem;

	// Dmem
	output [31:0] address_dmem, data;
	output wren;
	input [31:0] q_dmem;

	// Regfile
	output ctrl_writeEnable;
	output [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
	output [31:0] data_writeReg;
	input [31:0] data_readRegA, data_readRegB;


	/* YOUR CODE STARTS HERE */

    wire nclk = ~clock;
    wire [31:0] nop = 32'b0;

    // FETCH //
    wire [31:0] PC_main, PC_main_plus1;
    single_reg PC__main(PC_main, bex_PC, nclk, (!stall & !data_hazard), reset);
    assign PC_main_plus1 = PC_main + 1;
    // PC_incrementer(.sum(PC_main_plus1), .A(1), .B(PC_main), .c0(1'b0)); // increment PC by 1
    // bring PC to desired branch location if branch (determine in execute)
    wire [31:0] PC_branch_check, PC_j_check, PC_jr_check, branched_PC, j_PC, jr_PC, bex_PC;
    assign j_PC = FD_IR[16:0];
    assign jr_PC = data_readRegA;
    assign PC_j_check = (j_dec | jal_dec) ? j_PC : PC_main_plus1;
    assign PC_jr_check = jr_dec ? jr_PC : PC_j_check;
    assign PC_branch_check = isBranch ? branched_PC : PC_jr_check;
    assign bex_PC = rstatus_check ? target : PC_branch_check;
    assign branched_PC = DX_PC + immediate;

    // branch control logic - use isBranch to flush FD and DX latches (implemented in corresponding places)
    wire isBranch;
    assign isBranch = (bne && isNotEqual) || (blt && isLessThan); 
    wire rstatus_check;
    assign rstatus_check = bex && (DX_A != 0);

    // FD LATCH 
    wire [31:0] FD_IR, FD_PC, FD_PC_in, FD_IR_in, FD_IR_branch;
    assign address_imem = PC_main;
    assign FD_IR_branch = (isBranch | bex) ? nop : q_imem;
    assign FD_IR_in = stall ? FD_IR : FD_IR_branch;
    assign FD_PC_in = stall ? FD_PC : PC_main;
    single_reg FD__IR(FD_IR, FD_IR_in, nclk, (!stall && !data_hazard), reset);
    single_reg FD__PC(FD_PC, FD_PC_in, nclk, 1'b1, reset);

    // DECODE //

    wire arith_dec, addi_dec, sw_dec, lw_dec, j_dec, bne_dec, jal_dec, jr_dec, blt_dec, bex_dec, setx_dec, jumps_dec;
    opdecode dec_decoder(FD_IR[31:27], arith_dec, addi_dec, sw_dec, lw_dec, j_dec, bne_dec, jal_dec, jr_dec, blt_dec, bex_dec, setx_dec);
    assign jumps_dec = jal_dec | j_dec | jr_dec;
    
    wire [4:0] ctrl_readRegA_bex, ctrl_readRegB_1;
    assign ctrl_readRegA_bex = bex_dec ? 30 : FD_IR[21:17]; 
    assign ctrl_readRegA = (bne_dec | blt_dec | jr_dec) ? FD_IR[26:22] : ctrl_readRegA_bex;
    assign ctrl_readRegB_1 = (sw_dec) ? FD_IR[26:22] : FD_IR[16:12];
    assign ctrl_readRegB = (bne_dec | blt_dec) ? FD_IR[21:17] : ctrl_readRegB_1;
    // writeReg is decoded in writeback

    // DX latch
    wire [31:0] DX_A, DX_B, DX_IR, DX_PC, DX_IR_in_v1, DX_IR_in, DX_PC_in, DX_A_in, DX_B_in, DX_IR_branch, DX_A_jal, DX_B_jal, DX_A_nops, DX_B_nops;
    assign DX_IR_branch = (isBranch | jumps | bex | data_hazard) ? nop : FD_IR;
    assign DX_IR_in_v1 = stall ? DX_IR : DX_IR_branch;
    assign DX_IR_in = (data_hazard | j_dec | jr) ? nop : DX_IR_in_v1;
    assign DX_PC_in = stall ? DX_PC : FD_PC;
    assign DX_A_jal = jal_dec ? FD_PC : data_readRegA;
    assign DX_A_nops = (j_dec | data_hazard | jr) ? 0 : DX_A_jal;
    assign DX_A_in = stall ? DX_A : DX_A_nops;
    assign DX_B_jal = jal_dec ? 1 : data_readRegB;
    assign DX_B_nops = (j_dec | data_hazard | jr) ? 0 : DX_B_jal;
    assign DX_B_in = stall ? DX_B : DX_B_nops;
    single_reg DX__A(DX_A, DX_A_in, nclk, 1'b1, reset);
    single_reg DX__B(DX_B, DX_B_in, nclk, 1'b1, reset);
    single_reg DX__IR(DX_IR, DX_IR_in, nclk, 1'b1, reset);
    single_reg DX__PC(DX_PC, DX_PC_in, nclk, 1'b1, reset);

    // EXECUTE //

    // determine control signals from IR

    wire[4:0] opcode, shamt, ALU_op;
    wire [16:0] target;
    assign opcode = DX_IR[31:27];
    assign shamt = DX_IR[11:7];
    assign ALU_op = (opcode == 0) ? DX_IR[6:2] : 0;
    assign target = DX_IR[16:0];

    // instruction indicators
    wire arith, addi, sw, lw, j, bne, jal, jr, blt, bex, setx, add, sub, _and, _or, sll, sra, mult, div, branches, jumps, is_itype, multdiv;
    opdecode decoder(opcode, arith, addi, sw, lw, j, bne, jal, jr, blt, bex, setx);
    assign add = ALU_op == 5'b00000;
    assign sub = ALU_op == 5'b00001;
    assign _and = ALU_op == 5'b00010;
    assign _or = ALU_op == 5'b00011;
    assign sll = ALU_op == 5'b00100;
    assign sra = ALU_op == 5'b00101;
    assign mult = (ALU_op == 5'b00110);
    assign div = ALU_op == 5'b00111;
    assign branches = bne | blt | bex;
    assign jumps = j | jal | jr;
    assign is_itype = addi | sw | lw | bne | blt;
    assign multdiv = mult | div;

    // extract immediate val from i-types + sign-extend
    wire[31:0] immediate;
    wire imm_signbit;
    assign imm_signbit = DX_IR[16];
    assign immediate[31:17] = {15{imm_signbit}};
    assign immediate[16:0] = DX_IR[16:0];


    wire [31:0] ALUinA, ALUinB, ALUinA_byp, ALUinB_MXbyp, ALUinB_WXbyp;
    assign ALUinA_byp = ALUinA_MXbypass_log ? XM_O : DX_A;
    assign ALUinA = ALUinA_WXbypass_log ? MW_O : ALUinA_byp;

    assign ALUinB_MXbyp = ALUinB_MXbypass_log ? XM_O : DX_B;
    assign ALUinB_WXbyp = ALUinB_WXbypass_log ? MW_O : ALUinB_MXbyp;
    assign ALUinB = (sw | lw | addi | setx) ? immediate : ALUinB_WXbyp;

    wire [4:0] ALUop_in, ALUop_in2;
    assign ALUop_in = blt ? 5'b00001 : ALU_op;
    assign ALUop_in2 = (addi | setx | jal | sw | lw) ? 5'b0 : ALUop_in;

    wire [31:0] alu_out;
    wire alu_ovf, isNotEqual, isLessThan;
    alu cpu_alu(.data_operandA(ALUinA), .data_operandB(ALUinB), .ctrl_ALUopcode(ALUop_in2), .ctrl_shiftamt(shamt),
                .data_result(alu_out), .overflow(alu_ovf), .isNotEqual(isNotEqual), .isLessThan(isLessThan));
    
    // if mult or div = stall until dataready!
    wire [31:0] multdiv_out;
    wire mult_in, div_in, mult_ctrl_hold, div_ctrl_hold;
    assign mult_in = mult & !mult_ctrl_hold & !multdiv_ready;
    assign div_in = div & !div_ctrl_hold & !multdiv_ready;
    dffe_ref hold_mult_ctrl(mult_ctrl_hold, mult, clock, !multdiv_ready, multdiv_ready);
	dffe_ref hold_div_ctrl(div_ctrl_hold, div, clock, !multdiv_ready, multdiv_ready);
    wire multdiv_exception, multdiv_ready;
    multdiv multidivver(.data_operandA(ALUinA), .data_operandB(ALUinB),
                        .ctrl_MULT(mult_in), .ctrl_DIV(div_in),
                        .clock(clock), .data_result(multdiv_out),
                        .data_exception(multdiv_exception), .data_resultRDY(multdiv_ready));
    wire stall;
    assign stall = multdiv && !multdiv_ready;

    // output value logic
    // if alu operation, then aluout
    // if multdiv, then multdiv
    wire [31:0] out1, out2, out3, out4, out5, out6, out7;
    assign out1 = (add | addi | sll | sra | sub | _and | _or | sw | lw | jal) ? alu_out : 32'b0;
    assign out2 = (mult | div) ? multdiv_out : out1;
    // status outs
    // 1: addi (alu_ovf when addi && alu_ovf)
    // 2: add  (alu_ovf when add && alu_ovf)
    // 3: sub  (alu_ovf when sub && alu_ovf)
    // 4: mult  (multdiv_exception when mult && multdiv_exception)
    // 5: div   (multdiv_exception when div && multdiv_exception)
    assign out3 = (addi && alu_ovf) ? alu_ovf : out2;
    assign out4 = (add && alu_ovf) ? alu_ovf : out3;
    assign out5 = (sub && alu_ovf) ? alu_ovf : out4;
    assign out6 = (mult && multdiv_exception) ? multdiv_exception : out5;
    assign out7 = (div && multdiv_exception) ? multdiv_exception : out6;
    

    // XM latch
    wire [31:0] XM_O, XM_B, XM_IR, XM_O_in, XM_B_in, XM_IR_in, XM_B_in_MXbyp, XM_B_in_WXbyp;
    assign XM_O_in = stall ? XM_O : out7;
    assign XM_B_in_MXbyp = sw & ALUinB_MXbypass_log ? XM_O : DX_B; 
    assign XM_B_in_WXbyp = sw & ALUinB_WXbypass_log ? MW_O : XM_B_in_MXbyp; 
    assign XM_B_in = stall ? XM_B : XM_B_in_WXbyp;
    assign XM_IR_in = stall ? XM_IR : DX_IR;
    single_reg XM__O(XM_O, XM_O_in, nclk, 1'b1, reset);
    single_reg XM__B(XM_B, XM_B_in, nclk, 1'b1, reset);
    single_reg XM__IR(XM_IR, XM_IR_in, nclk, 1'b1, reset);


    // MEMORY //

    // redecode
    wire arith_mem, addi_mem, sw_mem, lw_mem, j_mem, bne_mem, jal_mem, jr_mem, blt_mem, bex_mem, setx_mem, mult_mem, div_mem;
    opdecode mem_decoder(XM_IR[31:27], arith_mem, addi_mem, sw_mem, lw_mem, j_mem, bne_mem, jal_mem, jr_mem, blt_mem, bex_mem, setx_mem);
    assign mult_mem = XM_IR[6:2] == 5'b00110; 
    assign div_mem = XM_IR[6:2] == 5'b00111;

    assign address_dmem = XM_O;
    assign data = WM_byp ? MW_D : XM_B;
    assign wren = sw_mem;

    // MW latch
    wire [31:0] MW_O, MW_D, MW_IR, MW_O_in, MW_D_in, MW_IR_in;
    assign MW_O_in = stall ? MW_O : XM_O;
    assign MW_D_in = stall ? MW_D : q_dmem;
    assign MW_IR_in = stall ? MW_IR : XM_IR;
    single_reg MW__O(MW_O, MW_O_in, nclk, 1'b1, reset);
    single_reg MW__B(MW_D, MW_D_in, nclk, 1'b1, reset);
    single_reg MW__IR(MW_IR, MW_IR_in, nclk, 1'b1, reset);


    // WRITEBACK //

    //redecode
    wire arith_wr, addi_wr, sw_wr, lw_wr, j_wr, bne_wr, jal_wr, jr_wr, blt_wr, bex_wr, setx_wr;
    opdecode wr_decoder(MW_IR[31:27], arith_wr, addi_wr, sw_wr, lw_wr, j_wr, bne_wr, jal_wr, jr_wr, blt_wr, bex_wr, setx_wr);

    wire [31:0] setx_writeReg;
    assign setx_writeReg = setx_wr ? 30 : MW_IR[26:22];
    assign ctrl_writeReg = jal_wr ? 31 : setx_writeReg;
    assign data_writeReg = lw_wr ? MW_D : MW_O;
    assign ctrl_writeEnable = arith_wr | addi_wr | lw_wr | jal_wr | setx_wr;


    // stall logic with bypassing
    wire data_hazard, jr_hazard;
    assign data_hazard = (lw && ((fd_sourceA == dx_dest) || ((fd_sourceB == dx_dest) && (!sw_dec)))) || jr_hazard;
    assign jr_hazard = jr_dec && ((fd_sourceB == dx_dest) || (fd_sourceB == xm_dest));

    // not used, reference for stalls and bypasses in gtkwave
    wire hcheck1, hcheck2, hcheck3, hcheck4;    
    assign hcheck1 = (fd_sourceA == dx_dest) && (fd_sourceA != 0) && (arith | addi | lw | jal);
    assign hcheck2 = (fd_sourceB == dx_dest) && (fd_sourceB != 0) && (arith | addi | lw | jal);
    assign hcheck3 = (fd_sourceA == xm_dest) && (fd_sourceA != 0) && (arith_wr | addi_wr | lw_wr | jal_wr);
    assign hcheck4 = (fd_sourceB == xm_dest) && (fd_sourceB != 0) && (arith_wr | addi_wr | lw_wr | jal_wr);


    wire [4:0] fd_sourceA, fd_sourceB, dx_sourceA, dx_sourceB, xm_sourceA, xm_sourceB, dx_dest, xm_dest, mw_dest;
    assign fd_sourceA = FD_IR [21:17];
    assign fd_sourceB = (sw_dec | bne_dec | blt_dec | jr_dec) ? FD_IR[26:22] : FD_IR[16:12];
    assign dx_sourceA = DX_IR [21:17];
    assign dx_sourceB = (sw | bne | blt | jr) ? DX_IR[26:22] : DX_IR[16:12];
    assign xm_sourceA = XM_IR [21:17];
    assign xm_sourceB = (sw_mem | bne_mem | blt_mem | jr_mem) ? XM_IR[26:22] : XM_IR[16:12];
    assign dx_dest = DX_IR[26:22];
    assign xm_dest = XM_IR[26:22];
    assign mw_dest = MW_IR[26:22];

    // MX bypass detection
    // if source for DX updated in XM, loop through
    // inst must be output of ALU AND be storing it back in regfile --> arith, lw, jal
    wire ALUinA_MXbypass_log, ALUinB_MXbypass_log;
    assign ALUinA_MXbypass_log = (arith_mem | addi_mem | lw_mem) && (xm_dest == dx_sourceA) && (DX_IR != nop && XM_IR != nop) && (xm_dest != 0); 
    assign ALUinB_MXbypass_log = (arith_mem | addi_mem | lw_mem | sw_mem) && (arith | sw) && (xm_dest == dx_sourceB) && (DX_IR != nop && XM_IR != nop) && (xm_dest != 0); 

    // WX bypass
    wire ALUinA_WXbypass_log, ALUinB_WXbypass_log;
    assign ALUinA_WXbypass_log = (arith_wr | addi_wr | lw_wr) && (mw_dest == dx_sourceA) && (DX_IR != nop && MW_IR != nop) && (mw_dest != 0);
    assign ALUinB_WXbypass_log = (arith_wr | addi_wr | lw_wr) && (arith | sw) && (mw_dest == dx_sourceB) && (DX_IR != nop && MW_IR != nop) && (mw_dest != 0); 

    // WM bypass
    wire WM_byp, bypass;
    assign WM_byp = lw_wr && sw_mem && (mw_dest == xm_sourceB) && (XM_IR != nop) && (MW_IR != nop) && (mw_dest != 0);
    assign bypass = ALUinA_MXbypass_log || ALUinB_MXbypass_log || ALUinA_WXbypass_log || ALUinB_WXbypass_log || WM_byp;

endmodule

    // data hazard detection - if FD source reg == DX or XM destination reg
        // sw, bne, blt, jal, use [26:22] as a source reg
    // latter stage only relevant for writeback instructions
        // add, addi, sub, and, or, sll, sra, mul, div, lw, jal
        // (arith_wr | addi_wr | lw_wr | jal_wr)
     //assign data_hazard = (hcheck1 || hcheck2 || hcheck3 || hcheck4) && (FD_IR != nop) && (DX_IR != nop || XM_IR != nop);
    // fetch source


// random number geneartor (inversion)

