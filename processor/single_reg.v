module single_reg(out, in, clock, input_enable, reset);
    input clock, input_enable, reset;
    input [31:0] in;
    output [31:0] out;

    // d flip flop every bit in the register
    
    dffe_ref bit_0(out[0], in[0], clock, input_enable, reset);
    dffe_ref bit_1(out[1], in[1], clock, input_enable, reset);
    dffe_ref bit_2(out[2], in[2], clock, input_enable, reset);
    dffe_ref bit_3(out[3], in[3], clock, input_enable, reset);
    dffe_ref bit_4(out[4], in[4], clock, input_enable, reset);
    dffe_ref bit_5(out[5], in[5], clock, input_enable, reset);
    dffe_ref bit_6(out[6], in[6], clock, input_enable, reset);
    dffe_ref bit_7(out[7], in[7], clock, input_enable, reset);
    dffe_ref bit_8(out[8], in[8], clock, input_enable, reset);
    dffe_ref bit_9(out[9], in[9], clock, input_enable, reset);
    dffe_ref bit_10(out[10], in[10], clock, input_enable, reset);
    dffe_ref bit_11(out[11], in[11], clock, input_enable, reset);
    dffe_ref bit_12(out[12], in[12], clock, input_enable, reset);
    dffe_ref bit_13(out[13], in[13], clock, input_enable, reset);
    dffe_ref bit_14(out[14], in[14], clock, input_enable, reset);
    dffe_ref bit_15(out[15], in[15], clock, input_enable, reset);
    dffe_ref bit_16(out[16], in[16], clock, input_enable, reset);
    dffe_ref bit_17(out[17], in[17], clock, input_enable, reset);
    dffe_ref bit_18(out[18], in[18], clock, input_enable, reset);
    dffe_ref bit_19(out[19], in[19], clock, input_enable, reset);
    dffe_ref bit_20(out[20], in[20], clock, input_enable, reset);
    dffe_ref bit_21(out[21], in[21], clock, input_enable, reset);
    dffe_ref bit_22(out[22], in[22], clock, input_enable, reset);
    dffe_ref bit_23(out[23], in[23], clock, input_enable, reset);
    dffe_ref bit_24(out[24], in[24], clock, input_enable, reset);
    dffe_ref bit_25(out[25], in[25], clock, input_enable, reset);
    dffe_ref bit_26(out[26], in[26], clock, input_enable, reset);
    dffe_ref bit_27(out[27], in[27], clock, input_enable, reset);
    dffe_ref bit_28(out[28], in[28], clock, input_enable, reset);
    dffe_ref bit_29(out[29], in[29], clock, input_enable, reset);
    dffe_ref bit_30(out[30], in[30], clock, input_enable, reset);
    dffe_ref bit_31(out[31], in[31], clock, input_enable, reset);
    
endmodule

// attempted genvar loop to instantiate d flip flops
    /*
    wire clock_en;
    and (clock_en, input_enable, clock);
    
    genvar i;
    generate
        for (i = 0; i < 32; i = i + 1) begin: loop1
            DFF a_dff(.D(in[i]), .clk(clock_en), .reset(reset), .Q(out[i]));
            // DFF(D, clk, reset, Q);
        end
    endgenerate
    */