module block_finder (
    input [9:0] target_x,
    input [9:0] target_y,
    input [9:0] square1x,
    input [9:0] square1y,
    input [9:0] square2x,
    input [9:0] square2y,
    input [9:0] square3x,
    input [9:0] square3y,
    input [9:0] square4x,
    input [9:0] square4y,
    input [9:0] square5x,
    input [9:0] square5y,
    input [9:0] square6x,
    input [9:0] square6y,
    input [9:0] square7x,
    input [9:0] square7y,
    input [9:0] square8x,
    input [9:0] square8y,
    output reg [2:0] block_number
);

    // target_x and target_y are some coordinate
    // new_block_x and new_block_y are the new destination coordinates
    // block_number determines which square holds target_x and target_y
    // what is returned/outputed is the square coordinates that need to be changed 

    always @* begin
        if (target_x == square1x && target_y == square1y)
            block_number <= 3'd1;
        else if (target_x == square2x && target_y == square2y)
            block_number <= 3'd2;
        else if (target_x == square3x && target_y == square3y)
            block_number <= 3'd3;
        else if (target_x == square4x && target_y == square4y)
            block_number <= 3'd4;
        else if (target_x == square5x && target_y == square5y)
            block_number <= 3'd5;
        else if (target_x == square6x && target_y == square6y)
            block_number <= 3'd6;
        else if (target_x == square7x && target_y == square7y)
            block_number <= 3'd7;
        else if (target_x == square8x && target_y == square8y)
            block_number <= 3'd8;
        else
            block_number <= 3'd0; // No block matches the target position
    end

endmodule