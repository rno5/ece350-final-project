module regfile(
	clock, ctrl_writeEnable, ctrl_reset, ctrl_writeReg,
	ctrl_readRegA, ctrl_readRegB, data_writeReg, data_readRegA,
	data_readRegB, reg1out, reg2out, reg3out, reg4out, reg5out, reg6out, reg7out, reg8out, 
	reg9out, reg10out, reg11out, reg12out, reg13out, reg14out, reg15out, reg16out);	
	
	input clock, ctrl_writeEnable, ctrl_reset;
	input [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
	input [31:0] data_writeReg;
	output [31:0] data_readRegA, data_readRegB;
	output [31:0] reg1out, reg2out, reg3out, reg4out, reg5out, reg6out, reg7out, reg8out, 
		   reg9out, reg10out, reg11out, reg12out, reg13out, reg14out, reg15out, reg16out;

	reg[31:0] registers[31:0];
	assign reg1out = registers[1];
	assign reg2out = registers[2];
	assign reg3out = registers[3];
	assign reg4out = registers[4];
	assign reg5out = registers[5];
	assign reg6out = registers[6];
	assign reg7out = registers[7];
	assign reg8out = registers[8];
	assign reg9out = registers[9];
	assign reg10out = registers[10];
	assign reg11out = registers[11];
	assign reg12out = registers[12];
	assign reg13out = registers[13];
	assign reg14out = registers[14];
	assign reg15out = registers[15];
	assign reg16out = registers[16];

	integer count;
	initial begin
		for (count=0; count<32; count=count+1)
			registers[count] <= 0;
	end

	integer i;
	always @(posedge clock or posedge ctrl_reset)
	begin
		if(ctrl_reset)
			begin
				for(i = 0; i < 32; i = i + 1)
					begin
						registers[i] <= 32'd0;
					end
			end
		else
			if(ctrl_writeEnable && ctrl_writeReg != 5'd0)
				registers[ctrl_writeReg] <= data_writeReg;
	end
	
	assign data_readRegA = registers[ctrl_readRegA];
	assign data_readRegB = registers[ctrl_readRegB];
	
endmodule