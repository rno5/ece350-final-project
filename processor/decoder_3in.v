module decoder_3in(left_shift, sub, add, opcode);
	/* take in 3 select bits, diff combos return diff instructions for main multiplier
	
    from lecture:
    – 000: middle of run of 0s, do nothing
	– 100: beginning of run of 1s, subtract multiplicand<<1 (M*2)
	– 010: singleton 1, add multiplicand
	– 110: beginning of run of 1s, subtract multiplicand
	– 001: end of run of 1s, add multiplicand
	– 101: end of run of 1s, beginning of another, subtract multiplicand
	– 011: end of a run of 1s, add multiplicand<<1 (M*2)
	– 111: middle of run of 1s, do nothing
	
    based on above:
    NOTHING: 000, 111 
    LEFT SHIFT MULTIPLICAND: 100, 011
    ADD: 010, 001, 011
    SUB: 100, 110, 101

    */

    input [2:0] opcode;
    output left_shift, sub, add;

    // NOTHING: 000, 111 --> !op0&!op1&!op2 | op0&op1&op2
        // DONT ACTUALLY NEED TO DO - can just bypass in main
    // and no0_(no0, !opcode[0], !opcode[1], !opcode[2]);
    // and no1_(no1, opcode[0], opcode[1], opcode[2]);
    // or no_vf(do_nothing, no0, no1);
    
    // LEFT SHIFT MULTIPLICAND: 100, 011 --> op0&!op1&!op2 | !op0&op1&op2
    wire sll0, sll1;
    and sll0_(sll0, opcode[2], ~opcode[1], ~opcode[0]);
    and sll1_(sll1, !opcode[2], opcode[1], opcode[0]);
    or sll_vf(left_shift, sll0, sll1);

    // ADD: 010, 001, 011 --> !op0&op1 | !op0&op2 = !op0 (op1 | op2)
    wire add0, add1, add2;
    and add0_(add0, ~opcode[2], opcode[1], ~opcode[0]); //010
    and add1_(add1, ~opcode[2], ~opcode[1], opcode[0]); //001
    and add2_(add2, ~opcode[2], opcode[1], opcode[0]); //011
    or adds(add, add0, add1, add2);

    // SUB: 100, 110, 101 --> op0&!op1 | op0&!op2 = op0 (!op1 | !op2)
    wire sub0, sub1, sub2;
    and sub0_(sub0, opcode[2], ~opcode[1], ~opcode[0]); //100
    and sub1_(sub1, opcode[2], opcode[1], ~opcode[0]); //110
    and sub2_(sub2, opcode[2], ~opcode[1], opcode[0]); //101
    or subs(sub, sub0, sub1, sub2);

endmodule