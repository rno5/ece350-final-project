module TFF(Q, T, clock, reset);
    // implemented using diagram in lecture
    input T, clock, reset;
    output Q;

    wire Q_temp, D, and1, and2;

    and and1_(and1, ~T, Q_temp);
    and and2_(and2, T, ~Q_temp);
    or or_(D, and1, and2);

    dffe_ref dff(.q(Q_temp),.d(D), .clk(clock), .en(1'b1), .clr(reset));

    assign Q = Q_temp;

endmodule