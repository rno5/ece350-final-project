// example of a complete 32-bit 2:1 mux
module mux_2 (out, select, in1, in0);
    input select;
    input in1, in0;
    output out;
    assign out = select ? in1 : in0;
endmodule
