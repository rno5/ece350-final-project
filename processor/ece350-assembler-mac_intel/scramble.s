# r1&r2 = block 1, r3&r4 = block 2, r5&r6 = block 3, r7&r8 = block 4, 
# r9&r10 = block 5, r11&r12 = block 6, r13&r14 = block 7, r15&r16 = block 8
# regs point to top left corner of each block (x, y) = (rOdd, rEven)
# goal: scramble such that order of blocks is now 4, 6, 8, 7, 3, 1, 2, 5

delay:
    addi   $r20, $r0, 10000

delay_loop:
    addi   $r20, $r20, -1
    bne    $r20, $r0, delay_loop
    jr     $ra

# initialize block 1
addi    $r1, $r0, 100
addi    $r2, $r0, 0

# initialize block 2
addi    $r3, $r0, 260
addi    $r4, $r0, 0

# initialize block 3
addi    $r5, $r0, 420
addi    $r6, $r0, 0

# initialize block 4
addi    $r7, $r0, 100
addi    $r8, $r0, 160

# initialize block 5
addi    $r9, $r0, 260
addi    $r10, $r0, 160

# initialize block 6
addi    $r11, $r0, 420
addi    $r12, $r0, 160

# initialize block 7
addi    $r13, $r0, 100
addi    $r14, $r0, 320

# initialize block 8
addi    $r15, $r0, 260
addi    $r16, $r0, 320

# show the initialized blocks
jal     delay

#######################################################################################

# move block 4
addi    $r7, $r7, 0
addi    $r8, $r8, -160

# move block 6
addi    $r11, $r11, -160
addi    $r12, $r12, -160

# move block 8
addi    $r15, $r15, 160
addi    $r16, $r16, -320

# move block 7
addi    $r13, $r13, 0
addi    $r14, $r14, -160

# move block 3
addi    $r5, $r5, -160
addi    $r6, $r6, 160

# move block 1
addi    $r1, $r1, 320
addi    $r2, $r2, 160

# move block 2
addi    $r3, $r3, -160
addi    $r4, $r4, 320

# move block 5
addi    $r9, $r9, 0
addi    $r10, $r10, 160